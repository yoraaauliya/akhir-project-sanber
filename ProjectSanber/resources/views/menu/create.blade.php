@extends('layout.master')
@section('judul')
Data Menu
@endsection

@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label >Nama Menu :</label>
      <input type="text" class="form-control" name="namaMenu">
    </div>
    @error('namaMenu')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label >Tipe Menu :</label>
      <input type="text" class="form-control" name="tipeMenu">
    </div>
    @error('tipeMenu')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >Bahan Utama :</label>
        <input type="text" class="form-control" name="bahanUtama">
    </div>
    @error('bahanUtama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >harga Menu :</label>
        <input type="number" class="form-control" name="hargaMenu">
    </div>
    @error('hargaMenu')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label >informasi Makanan :</label>
        <input type="text" class="form-control" name="informasiMakanan">
    </div>
    @error('informasiMakanan')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection