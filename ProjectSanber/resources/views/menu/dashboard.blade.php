@extends('layout.master')
@section('judul')
Menu Makanan
@endsection

@section('content')
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Menu</th>
        <th scope="col">Tipe Menu</th>
        <th scope="col">Bahan Utama</th>
        <th scope="col">Harga</th>
        <th scope="col">Informasi Makanan</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->namaMenu}}</td>
                <td>{{$item->tipeMenu}}</td>
                <td>{{$item->bahanUtama}}</td>
                <td>{{$item->hargaMenu}}</td>
                <td>{{$item->informasiMakanan}}</td>
                
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="#" class="btn btn-sm btn-warning">Add to Cart</a>
                    </form>
                </td>
            </tr>
            
        @empty
            <tr>
                <td>Data Kategori Kosong</td>
            </tr>
        @endforelse
    </tbody>

  </table>
@endsection