@extends('layout.master')
@section('judul')
Manage Menu
@endsection

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Menu</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Menu</th>
        <th scope="col">Tipe Menu</th>
        <th scope="col">Bahan Utama</th>
        <th scope="col">Harga</th>
        <th scope="col">Informasi Makanan</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$item->namaMenu}}</td>
                <td>{{$item->tipeMenu}}</td>
                <td>{{$item->bahanUtama}}</td>
                <td>{{$item->hargaMenu}}</td>
                <td>{{$item->informasiMakanan}}</td>
                
                <td>
                    <form action="/cast/{{$item->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">Edit</a>
                        <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                    </form>
                </td>
            </tr>
            
        @empty
            <tr>
                <td>Data Kategori Kosong</td>
            </tr>
        @endforelse
    </tbody>

  </table>
@endsection