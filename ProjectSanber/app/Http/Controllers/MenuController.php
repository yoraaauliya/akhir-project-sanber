<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function create()
    {
        return view('menu.create');
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            'namaMenu' => 'required',
            'tipeMenu' => 'required',
            'bahanUtama' => 'required',
            'hargaMenu' => 'required',
            'informasiMakanan' => 'required',
        ],
        [
            'namaMenu.required' => 'inputan nama harus diisi/tidak boleh kosong',
            'tipeMenu.required' => 'inputan tipe harus diisi/tidak boleh kosong',
            'bahanUtama.required' => 'inputan bahan harus diisi/tidak boleh kosong',
            'hargaMenu.required' => 'inputan harga harus diisi/tidak boleh kosong',
            'informasiMakanan.required' => 'inputan informasi harus diisi/tidak boleh kosong',
        ]
        );

        DB::table('menu')->insert(
            [
                'namaMenu' => $request['namaMenu'], 
                'tipeMenu' => $request['tipeMenu'],
                'bahanUtama' => $request['bahanUtama'],
                'hargaMenu' => $request['hargaMenu'],
                'informasiMakanan' => $request['informasiMakanan'],
                ]
        );

        return redirect('/cast');
        
    }

    public function index()
    {
        $cast = DB::table('menu')->get();
 
        return view('menu.index', compact('cast'));
    }

    public function dashboard()
    {
        $cast = DB::table('menu')->get();
 
        return view('menu.dashboard', compact('cast'));
    }

    public  function show($id)
    {
        $cast = DB::table('menu')->where('id', $id)->first();
        return view('menu.show', compact('cast'));

    }

    public function edit($id)
    {
        $cast = DB::table('menu')->where('id', $id)->first();
        return view('menu.edit', compact('cast'));

    }

    public function update($id, Request $request)
    {
        $request->validate([
            'namaMenu' => 'required',
            'tipeMenu' => 'required',
            'bahanUtama' => 'required',
            'hargaMenu' => 'required',
            'informasiMakanan' => 'required',
        ],
        [
            'namaMenu.required' => 'inputan nama harus diisi/tidak boleh kosong',
            'tipeMenu.required' => 'inputan tipe harus diisi/tidak boleh kosong',
            'bahanUtama.required' => 'inputan bahan harus diisi/tidak boleh kosong',
            'hargaMenu.required' => 'inputan harga harus diisi/tidak boleh kosong',
            'informasiMakanan.required' => 'inputan informasi harus diisi/tidak boleh kosong',
        ]
        );

        

        DB::table('menu')
              ->where('id', $id)
              ->update(
                [
                    'namaMenu' => $request['namaMenu'], 
                    'tipeMenu' => $request['tipeMenu'],
                    'bahanUtama' => $request['bahanUtama'],
                    'hargaMenu' => $request['hargaMenu'],
                    'informasiMakanan' => $request['informasiMakanan'],
                ]
                );

        return redirect('/cast');

    }

    public function destroy($id)
    {
        
        DB::table('menu')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
